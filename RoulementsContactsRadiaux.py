import os, sys, platform
import cairo
from os.path import join
import Errors
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
# from gi.repository import Pango
# import re
# Coefficients pour le calcul des roulements
coef_faoverc0 = (0.014, 0.028, 0.056, 0.084, 0.11, 0.17, 0.28, 0.42, 0.56)
coef_e =        (0.190, 0.220, 0.260, 0.280, 0.30, 0.34, 0.38, 0.42, 0.44)
coef_y =        (2.300, 1.990, 1.710, 1.550, 1.45, 1.31, 1.15, 1.04, 1)
# Caractéristiques des roulements
d,D,B,Nmax,Serie,C,C0 = [], [], [], [], [], [], []
Pr, val_e, val_y = 0,0,0
n, cas=0, 0
roulement = 51
Debug = False # debug de la partie calcul
numversion = "1.0"
dateversion = "01/2020"


def RepLancement():
    # en cas d'usage de PyInstaller pour créer un exe unique, il faut adapter le chemin du fichier ui,CSS
    if getattr(sys, 'frozen', False):
        wd = sys._MEIPASS
        print("bundle : path = ", wd,"plateforme :",platform.system())
        return wd
    else:
        wd = os.getcwd()
        print("live : path = ", wd,"plateforme :",platform.system())
        return wd

def chargebibliothequeroulements():
    global d, D, B, Nmax, Serie, C, C0, Debug
    # Charger la liste des roulements
    # Chaque ligne à la forme suivante "d=3 D=9 B=3 Nmax=40000 Série=10 C=500 C0=200"
    mon_fichier = open(wd + "\\" + "DB_roulements_billes.txt", "r")
    contenu = mon_fichier.readlines()
    i = 0
    for line in contenu:
        d.insert(i, line[2:line.find("D=") - 1])
        D.insert(i, line[line.find("D=") + 2:line.find("B=") - 1])
        B.insert(i, line[line.find("B=") + 2:line.find("Nmax=") - 1])
        Nmax.insert(i, line[line.find("Nmax=") + 5:line.find("Série=") - 1])
        Serie.insert(i, line[line.find("Série=") + 6:line.find("C=") - 1])
        C.insert(i, line[line.find("C=") + 2:line.find("C0=") - 1])
        C0.insert(i, line[line.find("C0=") + 3:].rstrip('\n'))
        i += 1
    mon_fichier.close()

def initcaracteristiquesroulement(self, r):
    chargebibliothequeroulements()
    # mise a jour des labels de l'interface graphique afin d'avoir l'affichage des résultats
    self.calc_label_d.set_label(str(d[r]))
    self.calc_label_D.set_label(str(D[r]))
    self.calc_label_B.set_label(str(B[r]))
    self.calc_label_Nmax.set_label(str(Nmax[r]))
    self.calc_label_Serie.set_label(str(Serie[r]))
    self.calc_label_C.set_label(str(C[r]))
    self.calc_label_C0.set_label(str(C0[r]))

    #self.pre_C.set_label(str(round(Cmin)))

def duree (Fa, Fr, faoverc0, C, Freq):
    global val_e, val_y, Pr, L10hr, n, cas
    Debug == True and print("Fr={0} - Fa={1} - Freq={2} - Fa/Fr={3} - Fa/C0={4}".format(Fr, Fa, Freq, Fa/Fr, faoverc0))
    if Fa == 0:
        cas=1
        Pr = Fr
    else:
        # Determination des coefficients e et y plus un indice n
        if faoverc0 < coef_faoverc0[0]:
            # cas extreme
            n=-1
            val_e = coef_e[0]
            val_y = coef_y[0]
            Debug == True and print("0- : e={0} - y={1}".format(val_e,val_y))
        for i in range(0,8):
            if faoverc0 >= coef_faoverc0[i] and faoverc0 < coef_faoverc0[i+1]:
                n = i
                val_e = coef_e[n] + (coef_e[n+1] - coef_e[n]) * (faoverc0 - coef_faoverc0[n]) / (coef_faoverc0[n+1] - coef_faoverc0[n])
                val_y = coef_y[n] + (coef_y[n+1] - coef_y[n]) * (faoverc0 - coef_faoverc0[n]) / (coef_faoverc0[n+1] - coef_faoverc0[n])
                Debug == True and print("{2} : e={0} - y={1}".format(val_e, val_y,n))
        if faoverc0 > coef_faoverc0[8]:
            # cas extreme
            n=8
            val_e = coef_e[8]
            val_y = coef_y[8]
            Debug == True and print("8+ : e={0} - y={1}".format(val_e, val_y))

        # calcul de P à partir des coefs
        if Fa/Fr <= val_e:
            cas=1
            Pr = Fr
        else:
            cas=2
            Pr = 0.56 * Fr + val_y * Fa
    L10hr = 1000000/(60*float(Freq))*(float(C)/Pr)**3

class Interface:
    __gtype_name__ = "RoulementsContactsRadiaux V1.0 du 012020A"

    def __init__(self):
        global wd
        wd = RepLancement()

        file_path = os.path.join(wd, 'InterfaceRoulements.glade')
        interface = Gtk.Builder()
        interface.add_from_file(file_path)
        # Create buffer
        self.double_buffer = None
        # connect signals
        interface.connect_signals(self)
        window = interface.get_object("window")
        self.set_style()
        self.notebook = interface.get_object("notebook1")
        self.version = interface.get_object("Version")
        self.version.set_label("Version {0} du {1}".format(numversion,dateversion))
        # Volet calcul
        self.calc_Fr = interface.get_object("calc_Fr")
        self.calc_Fa = interface.get_object("calc_Fa")
        self.calc_N = interface.get_object("calc_N")
        self.calc_label_d = interface.get_object("calc_label_d")
        self.calc_label_D = interface.get_object("calc_label_D")
        self.calc_label_B = interface.get_object("calc_label_B")
        self.calc_label_Nmax = interface.get_object("calc_label_Nmax")
        self.calc_label_Serie = interface.get_object("calc_label_Serie")
        self.calc_label_C = interface.get_object("calc_label_C")
        self.calc_label_C0 = interface.get_object("calc_label_C0")
        self.calc_label_FaoverFr = interface.get_object("calc_label_FaoverFr")
        self.calc_label_FaoverC0 = interface.get_object("calc_label_FaoverC0")
        self.calc_label_e = interface.get_object("calc_label_e")
        self.calc_label_y = interface.get_object("calc_label_y")
        self.calc_label_P = interface.get_object("calc_label_P")
        self.calc_label_L10h = interface.get_object("calc_label_L10h")

        # Volet prédétermination
        self.pre_Fr = interface.get_object("pre_entry_Fr")
        self.pre_Fa = interface.get_object("pre_entry_Fa")
        self.pre_N = interface.get_object("pre_entry_N")
        self.pre_L10h = interface.get_object("pre_entry_L10h")
        self.pre_P = interface.get_object("pre_label_P")
        self.pre_C = interface.get_object("pre_label_C")
        self.pre_text = interface.get_object("pre_textview")
        self.buffer = self.pre_text.get_buffer()
        window.show_all()

    # Lien avec glade : notebook1 / signaux / GTKnoteBook/ switch-page -> on_notebook_switchpage
    def on_notebook_switchpage(self, notebook, page, page_num, data=None):
        self.tab = notebook.get_nth_page(page_num)
        self.label = notebook.get_tab_label(self.tab).get_label()
        print(self.label)

    # lien avec glade : ver_bt_calc (bouton) / Signaux / GtkButton / clicked -> ver_bt_calc_clicked
    def calc_bt_calc_clicked(self, widget):
        # CalculCoussinet.Verif(self)
        print("calcul")
        Erreur = False
        # collecte du contenu des champs de l'interface graphique
        Fr = self.calc_Fr.get_text()
        Fa = self.calc_Fa.get_text()
        N = self.calc_N.get_text()
        # vérifier que les champs sont numériques et pour certains, non nul
        for text in (Fr, Fa, N):
            if not Errors.is_numeric(text):
                Errors.MessageErreur()
                Erreur = True
                break
            else:
                if Fr == "0":
                    Errors.MessageErreurZero();
                    erreur = True
                    break
        if not Erreur:
            initcaracteristiquesroulement(self, roulement)
            duree (float(Fa), float(Fr), float(Fa)/float(C0[roulement]), C[roulement], N)
            self.calc_label_FaoverFr.set_label(str(float(Fa)/float(Fr)))
            self.calc_label_FaoverC0.set_label(str(round(float(Fa) / float(C0[roulement]),3)))
            self.calc_label_e.set_label(str(round(val_e,3)))
            self.calc_label_y.set_label(str(round(val_y,3)))
            self.calc_label_P.set_label(str(round(Pr,1)))
            self.calc_label_L10h.set_label(str(round(L10hr)))
            # encadré sur l'image du tableau + graphics
            db = self.double_buffer
            if db is not None:
                # Create cairo context with double buffer as is DESTINATION
                cc = cairo.Context(db)
                # Draw a white background
                cc.set_source_rgb(1, 1, 1)
                cc.paint()
                self.double_buffer = cairo.ImageSurface.create_from_png(wd + "\\" + "Coef calcul Roulements V2.png")
                cc.paint()
                cc.set_source_rgb(1, 1, 1)
                # Initialize the buffer
                db = self.double_buffer
                # Create cairo context with double buffer as is DESTINATION
                cc = cairo.Context(db)
                cc.set_source_rgb(1, 0, 0)
                line_width = 8.0
                line_width, notused = cc.device_to_user(line_width, 0.0)
                cc.rectangle(0, 0, 1294, 322)
                cc.set_line_width(line_width)
                cc.set_source_rgb(1, 0, 0)
                cc.stroke()
                # encadrer la zone
                line_width = 3.0
                line_width, notused = cc.device_to_user(line_width, 0.0)
                cc.set_line_width(line_width)
                if n==-1: cc.rectangle(130,179,2,138)
                if 0<=n<8:  cc.rectangle(132+n*129, 179, 258, 138)
                if n==8: cc.rectangle(1288,179,2,138)
                cc.set_source_rgb(0, 1, 0)
                # mettre un croix sur le cas
                cc.move_to(415+(cas-1)*795,90)
                cc.line_to(425+(cas-1)*795,100)
                cc.move_to(415+(cas-1)*795,100)
                cc.line_to(425+(cas-1)*795, 90)
                cc.stroke()
                # Flush drawing actions
                db.flush()

    # lien avec glade : combo_coussinet / signaux / GtkComboBox / changed -> rlt_coussinet_changed
    def combo_rlt_changed(self, widget, data=None):
        global roulement
        model = widget.get_model()
        active = widget.get_active()
        if active >= 0:
            temp = model[active][0]
            roulement = active
            print("code : ", temp, "n°", roulement)
            initcaracteristiquesroulement(self, roulement)
        else:
            print("erreur combo")


    # lien avec glade : ver_bt_calc (bouton) / Signaux / GtkButton / clicked -> ver_bt_calc_clicked
    def pre_bt_calc_clicked(self, widget):
        global wd
        # CalculCoussinet.Verif(self)
        print("précalcul")
        chargebibliothequeroulements()
        Erreur = False
        # collecte du contenu des champs de l'interface graphique
        Fr = self.pre_Fr.get_text();
        Fa = self.pre_Fa.get_text()
        N = self.pre_N.get_text()
        L10h = self.pre_L10h.get_text()
        # effacer le GtkTextView (le buffer)
        self.buffer.delete(self.buffer.get_start_iter(),self.buffer.get_end_iter())
        # vérifier que les champs sont numérique et pour certains, non nul
        for text in (Fr, Fa, N, L10h):
            if not Errors.is_numeric(text):
                Errors.MessageErreur();
                Erreur = True
        if not Erreur:
            if float(Fa)==0:
                P=float(Fr)
            else:
                P=0.56*float(Fr)+2.3*float(Fa)
            Cmin=((float(L10h)*60*float(N))/1000000)**(1/3) * P
            print("P={0} - C={1}".format(P,Cmin))
            # mise a jour des labels de l'interface graphique afin d'avoir l'affichage des résultats
            self.pre_P.set_label(str(round(P)))
            self.pre_C.set_label(str(round(Cmin)))
            # liste des roulements qui fonctionnent
            i=0
            j=0
            for VarC in C:
                faoc0 = float(Fa) / float(C0[i])
                duree(float(Fa), float(Fr), faoc0, C[i], float(N))
                if float(L10h) <= L10hr:
                    rlt = "d={0} / D={1} / B={2} / Nmax={3} / C={4} - C0={5} - Fa/C0={6} - e={7}({11}) - y={8} /P={9} / L10h={10}\n".format(d[i], D[i], B[i],
                                                                                                 Nmax[i], C[i], C0[i],
                                                                                                 round(faoc0,3),round(val_e,3),round(val_y,3),round(Pr),round(L10hr),n)
                    self.buffer.insert_at_cursor(rlt)
                    j += 1 # compter les roulements
                i += 1
            self.buffer.insert_at_cursor(str(j) + " roulements qui conviennent.")


    def set_style(self):
        """
        Change Gtk+ Style
        """
        provider = Gtk.CssProvider()
        # Demo CSS kindly provided by Numix project
        provider.load_from_path(join(wd, 'style.css'))
        screen = Gdk.Display.get_default_screen(Gdk.Display.get_default())
        # I was unable to found instrospected version of this
        GTK_STYLE_PROVIDER_PRIORITY_APPLICATION = 600
        Gtk.StyleContext.add_provider_for_screen(
            screen, provider,
            GTK_STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    # lien avec glade GtkDrawingArea / Signaux / GtkWidget / draw -> on_draw
    def on_draw(self, widget, cr):
        # Throw double buffer into widget drawable
        if self.double_buffer is not None:
            cr.set_source_surface(self.double_buffer, 0, 0)
            cr.paint()
        else:
            print('Invalid double buffer')
        return False

    # lien avec glade GtkDrawingArea / Signaux / GtkWidget / configure-event -> on_configure
    def on_configure(self, widget, event, data=None):
        """Configure the double buffer based on size of the widget"""
        # Destroy previous buffer
        if self.double_buffer is not None:
            self.double_buffer.finish()
            self.double_buffer = None

        # Create a new buffer
        self.double_buffer = cairo.ImageSurface.create_from_png(wd + "\\" + "Coef calcul Roulements V2.png")

        # Initialize the buffer
        db = self.double_buffer
        # Create cairo context with double buffer as is DESTINATION
        cc = cairo.Context(db)
        # Draw a white background
        cc.set_source_rgb(0.5, 0.5, 0.5)
        line_width = 10.0
        line_width, notused = cc.device_to_user(line_width, 0.0)
        cc.rectangle(0, 0, 1294, 322)
        cc.set_line_width(line_width)
        cc.set_source_rgb(1, 0, 0)
        cc.stroke()
        # Flush drawing actions
        db.flush()
        return False

    # Lien avec glade : windows / signaux / GtkObject / destroy -> destroy
    def destroy(self, widget):
        print("Au Revoir !")
        Gtk.main_quit()

if __name__ == "__main__":
    app = Interface()
    Gtk.main()